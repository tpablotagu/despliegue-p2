const model = require("./modelo");
const http = require("http");
const httpCodes = require("http-status-codes");
const configuration = require("./modelo/configuration");
const moment = require("moment");

let handleRequest = (request, response) => {
    let responseType = configuration.data.responseTypes.text;
    let responseContent = "";
    let responseCode = httpCodes.OK;
    let endpoint = "";
    let parameter = "";
    let index = request.url.lastIndexOf("/");
    
    let managedResponse = {
        responseCode: "",
        responseContent: ""
    };

    if(index>0){
        endpoint = request.url.substring(0, index).toLowerCase();
        parameter = request.url.substring(index+1).toLowerCase();
    }else{
        endpoint = request.url.toLowerCase();
    } 

    let loggedUser = isLoggedUser(request);
    let currentEndpoint = configuration.endpoints.find(x => x.endpoint == endpoint);
    let currentMethod;

    if(!currentEndpoint){
        response.writeHead(httpCodes.NOT_FOUND, {"Content-Type": responseType});    
        response.write(JSON.stringify({ error: true, mensajeError: "Recurso no disponible"}));        
        response.end();
    }else{
        currentMethod = currentEndpoint.methodsAllowed.find( x => x.method == request.method);
        if(!currentMethod){
            response.writeHead(httpCodes.METHOD_NOT_ALLOWED, {"Content-Type": responseType});    
            response.write(JSON.stringify({ error: true, mensajeError: "Método no disponible para este recurso"}));        
            response.end();
        }
        else{
            // Request with autentication
            if(currentMethod.needAuthentication && !loggedUser){
                response.writeHead(httpCodes.FORBIDDEN, {"Content-Type": responseType});    
                response.write(JSON.stringify({ error: true, mensajeError: "Usuario no autenticado"}));        
                response.end();
            }
            else{
                switch(currentEndpoint.endpoint){
                    case configuration.data.strEndpoints.login:
                        handleLoginRequest(request, response);
                        break;
                    case configuration.data.strEndpoints.task:
                        handleTaskRequest(request, response, loggedUser, currentMethod, parameter);
                        break;
                    case configuration.data.strEndpoints.tasks:
                        handleTasksRequest(request, response, loggedUser);
                        break;
                    case configuration.data.strEndpoints.activeTasks:
                        handleActiveTasksRequest(request, response, loggedUser, parameter);
                        break;          
                }
            }
        }
    }           
};


function handleLoginRequest(request, response){
    let body = [];
    request.on('data', (chunk) => {
        body.push(chunk);
    }).on('end', () => {
        body = Buffer.concat(body).toString();
        if(body != ""){
            body = JSON.parse(body);
        }
        model.User.validateUser(body)
            .then(resp => {
                if(resp.length>0){
                    let token = model.User.generateToken(body.login);
                    response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
                    response.write(JSON.stringify({
                        error: false,
                        token: token
                    })); 
                }else{
                    response.writeHead(httpCodes.UNAUTHORIZED, {"Content-Type": configuration.data.responseTypes.text});    
                    response.write(JSON.stringify({
                        error: true,
                        mensajeError: "Usuario o contraseña incorrectos"
                    })); 
                }
                response.end();    
            })
            .catch(error => {
                response.writeHead(httpCodes.INTERNAL_SERVER_ERROR, {"Content-Type": configuration.data.responseTypes.text});    
                response.write(JSON.stringify({
                    error: true,
                    mensajeError: "Se ha producido un error inesperado al realizar la autenticación"
                }));        
                response.end();    
            });     
    });
}

function handleTaskRequest(request, response, loggedUser, currentMethod, parameter){
    let nTask;
    let body = [];    
    switch(currentMethod.method){
        case configuration.data.methods.get:
            model.Task.findById(parameter, loggedUser.login).then(resp => {
                response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
                response.write(JSON.stringify({
                    token: model.User.generateToken(loggedUser.login),
                    resultado: resp
                }));        
                response.end();  
            }).catch(error => {
                response.writeHead(httpCodes.BAD_REQUEST, {"Content-Type": configuration.data.responseTypes.text});    
                response.write(JSON.stringify({
                    token: model.User.generateToken(loggedUser.login),
                    resultado: error
                }));        
                response.end();  
            });
        break;
        case configuration.data.methods.post:
            request.on('data', (chunk) => {
                body.push(chunk);
            }).on('end', () => {
                body = Buffer.concat(body).toString();
                if(body != ""){
                    body = JSON.parse(body);
                }
                nTask = new model.Task(body);
                nTask.user = loggedUser.login;
                nTask.create().then(resp => {
                    response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
                    response.write(JSON.stringify({
                        token: model.User.generateToken(loggedUser.login),
                        nuevoId: resp.insertId
                    }));        
                    response.end();  
                }).catch(error => {
                    response.writeHead(httpCodes.BAD_REQUEST, {"Content-Type": configuration.data.responseTypes.text});    
                    response.write(JSON.stringify({
                        token: model.User.generateToken(loggedUser.login),
                        resultado: error
                    }));        
                    response.end();  
                });
            });
            break;
        case configuration.data.methods.put:
            request.on('data', (chunk) => {
                body.push(chunk);
            }).on('end', () => {
                body = Buffer.concat(body).toString();
                if(body != ""){
                    body = JSON.parse(body);
                }
                nTask = new model.Task(body);
                nTask.user = loggedUser.login;
                nTask.update().then(resp => {
                    response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
                    response.write(JSON.stringify({
                        token: model.User.generateToken(loggedUser.login),
                        error: false,
                        mensajeError: "",
                        filasAfectadas: resp.affectedRows
                    }));        
                    response.end();  
                }).catch(error => {
                    response.writeHead(httpCodes.BAD_REQUEST, {"Content-Type": configuration.data.responseTypes.text});    
                    response.write(JSON.stringify({
                        token: model.User.generateToken(loggedUser.login),
                        error: true,
                        mensajeError: "Error al actualizar",
                        filasAfectadas: 0
                    }));       
                    response.end();  
                });
            });
            break;
        case configuration.data.methods.delete:
            nTask = new model.Task("");
            nTask.user = loggedUser.login;
            nTask.id = parameter;
            nTask.delete().then(resp => {
                response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
                response.write(JSON.stringify({
                    token: model.User.generateToken(loggedUser.login),
                    error: false,
                    mensajeError: "",
                    filasAfectadas: resp.affectedRows
                }));        
                response.end();  
            }).catch(error => {
                response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
                response.write(JSON.stringify({
                    token: model.User.generateToken(loggedUser.login),
                    error: true,
                    mensajeError: "Error al eliminar",
                    filasAfectadas: 0
                }));        
                response.end();  
            });
            break;
    }
}

function handleTasksRequest(request, response, loggedUser){
    model.Task.getAll(loggedUser.login).then(resp => {
        response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
        response.write(JSON.stringify({
            token: model.User.generateToken(loggedUser.login),
            resultado: resp
        }));        
        response.end();   
    }).catch(error => {
        response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
        response.write(JSON.stringify({
            token: model.User.generateToken(loggedUser.login),
            mensajeError: error
        }));        
        response.end();  
    });
}

function handleActiveTasksRequest(request, response, loggedUser, parameter){
    if(parameter && parameter !== ""){
        model.Task.getActivesByPriority(loggedUser.login, parameter).then(resp => {
            response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
            response.write(JSON.stringify({
                token: model.User.generateToken(loggedUser.login),
                resultado: resp
            }));        
            response.end();   
        }).catch(error => {
            response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
            response.write(JSON.stringify({
                token: model.User.generateToken(loggedUser.login),
                mensajeError: error
            }));        
            response.end();  
        });
    }else{
        model.Task.getActives(loggedUser.login).then(resp => {
            response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
            response.write(JSON.stringify({
                token: model.User.generateToken(loggedUser.login),
                resultado: resp
            }));        
            response.end();   
        }).catch(error => {
            response.writeHead(httpCodes.OK, {"Content-Type": configuration.data.responseTypes.text});    
            response.write(JSON.stringify({
                token: model.User.generateToken(loggedUser.login),
                mensajeError: error
            }));        
            response.end();  
        });
    }
}

function isLoggedUser(request){
    if(request.headers.authorization){
        return model.User.validateToken(request.headers.authorization);
    }
    return false;
}

http.createServer(handleRequest).listen(configuration.data.port);

//Task.getAll().then(resp => console.log(resp)).catch( resp => console.log(resp));


