const mysql = require('mysql');
const configuration = require("./configuration")

let connection = mysql.createConnection({
    host: configuration.data.database.host,
    user: configuration.data.database.user,
    password: configuration.data.database.pass,
    database: configuration.data.database.catalog
});

module.exports = connection;