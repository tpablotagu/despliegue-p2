const Task = require("./task.class");
const User = require("./user.class");

module.exports = {
    Task: Task,
    User: User
};