const jwt = require("jsonwebtoken");
const md5 = require("md5");
const connection = require("./bdconfig");
const configuration = require("./configuration");

class User{
    static validateUser(user){
        try {
            let md5Password = md5(user.password);
            user.password = md5Password;
            let strQuery = "SELECT login FROM usuarios WHERE login=? and password=?";
            return new Promise((resolve, reject) => {
                connection.query(strQuery, [user.login, user.password], (error, result, fields) => {
                    if (error){
                        return reject(error);
                    }
                    else{
                        return resolve(result);
                    }
                });
            });
        } catch (error) {
            console.log(error);
        }
    }

    static generateToken(login){
        let token =  jwt.sign({login: login}, configuration.data.secret, {expiresIn:"30 minutes"});
        return token;
    } 

    static validateToken(token){
        try {
            let resultado =  jwt.verify(token, configuration.data.secret);
            return resultado;
        } catch (error) {
            console.log(error);            
        }
        return false;
    }
}

module.exports= User;