const data = {
    dateFormat: "dd/MM/yyyy hh:mm",
    tasksFile: "./tareas.json",
    encoding: "utf8",
    secret: "D3sP|1eG_e",
    port: 8080,
    database: {
        host: "localhost",
        user: "desplieguep2",
        pass: "desp2017p2",
        catalog: "tareas"
    },
    priorities: {
        high: "alta",
        medium: "media",
        low: "baja"
    },
    responseTypes:{
        text: "text/plain",
        html: "text/html",
        jpg: "image/jpg",
        png: "image/png"
    },
    methods: {
        get: "GET",
        post: "POST",
        put: "PUT",
        delete: "DELETE"
    },
    strEndpoints: {
        login: "/login",
        task: "/tarea",
        tasks: "/tareas",
        activeTasks: "/tareas_vigentes",
    },
    
}

const endpoints = [{ 
        endpoint: data.strEndpoints.login,
        methodsAllowed: [{
            method: data.methods.post,
            needAuthentication: false   
    }]
    },{ 
        endpoint: data.strEndpoints.task,
        methodsAllowed: [{
            method: data.methods.post,
            needAuthentication: true
        },{
            method: data.methods.get,
            needAuthentication: true
        },{
            method: data.methods.put,
            needAuthentication: true
        },{
            method: data.methods.delete,
            needAuthentication: true
        }]
    },{ 
        endpoint: data.strEndpoints.tasks,
        methodsAllowed: [{
            method: data.methods.get,
            needAuthentication: true
        }]
    },{ 
        endpoint: data.strEndpoints.activeTasks,
        methodsAllowed: [{
            method: data.methods.get,
            needAuthentication: true
        }]
    }
]

module.exports = {
    data: data,
    endpoints: endpoints
}