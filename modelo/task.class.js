const connection = require("./bdconfig");

class Task {
    constructor(data){
        this.id = data.id;
        this.text = data.texto;
        this.date = data.fechaTope;
        this.priority = data.prioridad;
        this.user = data.usuario;
    }
    
    create(){
        return new Promise( (resolve, reject) => {
            let data = {
                id: this.id,
                texto: this.text,
                fechaTope: this.date,
                prioridad: this.priority,
                usuario: this.user
            };
            connection.query("INSERT INTO tareas SET ?", data, (error, result, fields) => {
                if (error)
                    return reject(error);
                else
                    return resolve(result);
            });
        });
    }
    
    delete(){
        return new Promise( (resolve, reject) => {
            connection.query("DELETE FROM tareas WHERE id=" + this.id + " AND usuario='" + this.user + "'", (error, result, fields) => {
                if (error)
                    return reject(error);
                else
                    return resolve(result);
            });
        });
    }
    
    update(){
        return new Promise( (resolve, reject) => {
            let data = {
                id: this.id,
                texto: this.text,
                fechaTope: this.date,
                prioridad: this.priority,
                usuario: this.user
            };
            connection.query("UPDATE tareas SET texto='"+ this.text + "', fechaTope='"+ this.date +
                "', prioridad='"+ this.priority + "' WHERE usuario='"+ this.user + "' and id=" + this.id,
                 (error, result, fields) => {
                    if (error)
                        return reject(error);
                    else
                        return resolve(result);
                }   
            );
        });
    }

    static findById(id, user){
        return new Promise( (resolve, reject) => {
            connection.query("SELECT * FROM tareas where id = '" + id +"' and usuario = '" + user + "'", 
                (error, result, fields) => {
                    if(error)
                        return reject(error);
                    else
                        resolve(result.map(row => new Task(row)));
                }
            );          
        });
    }

    static getAll(user){
        return new Promise( (resolve, reject) => {
            connection.query("SELECT * FROM tareas where usuario = '" + user +"'", (error, result, fields) => {
                if(error)
                    return reject(error);
                else
                    resolve(result.map(row => new Task(row)));
            });          
        });
    }

    static getActives(user){
        return new Promise( (resolve, reject) => {
            connection.query("SELECT * FROM tareas WHERE usuario='" + user +"' AND fechaTope>NOW()", (error, result, fields) => {
                if(error)
                    return reject(error);
                else
                    resolve(result.map(row => new Task(row)));
            });          
        });
    }

    static getActivesByPriority(user, priority){
        return new Promise( (resolve, reject) => {
            connection.query("SELECT * FROM tareas WHERE usuario='" + user +"' AND fechaTope>NOW() AND prioridad='" + priority + "'", (error, result, fields) => {
                if(error)
                    return reject(error);
                else
                    resolve(result.map(row => new Task(row)));
            });          
        });
    }
}


module.exports = Task;